﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using cardinal_client.managers;
using CitizenFX.Core;
using CitizenFX.Core.Native;

namespace cardinal_client
{
    public class Client : BaseScript
    {
        public static Client INSTANCE { get; set; }

        public Config ClientCfg = new Config();

        public ChatManager ChatManager = new ChatManager();

        public BallisticManager BallisticManager = new BallisticManager();
        public UniformManager UniformManager = new UniformManager();

        public BeltManager BeltManager = new BeltManager();

        public PoliceManager PoliceManager = new PoliceManager();
        public VehicleManager VehicleManager = new VehicleManager();

        public HudManager HudManager = new HudManager();

        public Client()
        {
            Client.INSTANCE = this;

            EventHandlers.Add("onClientMapStart", new Action(this.MapCallback));
            EventHandlers.Add("playerSpawned", new Action(this.SpawnCallback));

            EventHandlers.Add("crp:CL_E:Chat:DisplayMessage", new Action<int, string, dynamic, string>(this.ChatManager.DisplayMessage));
            EventHandlers.Add("crp:CL_E:Chat:HandleLocal", new Action<int, string, string>(this.ChatManager.LocalCallback));
            EventHandlers.Add("crp:CL_E:Chat:SendTweet", new Action<string>(this.ChatManager.SendTweet));
            EventHandlers.Add("crp:CL_E:Chat:SetTwitterHandle", new Action<string>(this.ChatManager.SetTwitterHandle));

            EventHandlers.Add("crp:CL_E:LoadConfig", new Action<string>(this.LoadConfig));
            EventHandlers.Add("crp:CL_E:Uniform:ToggleGloves", new Action(this.UniformManager.ToggleGloves));
            EventHandlers.Add("crp:CL_E:Uniform:ToggleHat", new Action<string>(this.UniformManager.ToggleHat));

            EventHandlers.Add("crp:CL_E:Ballistics:Revive", new Action(this.BallisticManager.Revive));
            EventHandlers.Add("crp:CL_E:Ballistics:Heal", new Action(this.BallisticManager.Heal));
            EventHandlers.Add("crp:CL_E:Ballistics:ReviveClosest", new Action(this.BallisticManager.ReviveClosest));
            EventHandlers.Add("crp:CL_E:Ballistics:HealClosest", new Action(this.BallisticManager.HealClosest));

            EventHandlers.Add("crp:CL_E:Police:ToggleCuff", new Action(this.PoliceManager.ToggleCuff));
            EventHandlers.Add("crp:CL_E:Police:ToggleHandsup", new Action(this.PoliceManager.ToggleHandsup));
            EventHandlers.Add("crp:CL_E:Police:CuffClosest", new Action(this.PoliceManager.CuffClosest));

            Tick += new Func<Task>(async delegate
            {
                this.BallisticManager.Tick();
                this.HudManager.RenderHud();
                this.VehicleManager.OnTick();
            });

            Tick += this.PoliceManager.OnTick;
        }

        public void MapCallback()
        {
            TriggerServerEvent("crp:SV_E:PlayerReady");

            Exports["spawnmanager"].spawnPlayer();
            Exports["spawnmanager"].setAutoSpawn(false);
        }

        public void SpawnCallback()
        {
            API.NetworkSetFriendlyFireOption(true);
            API.SetCanAttackFriendly(Game.PlayerPed.Handle, true, true);

            this.ChatManager.RegisterCommandTips();
        }

        private void LoadConfig(string cfgJson)
        {
            if(!(String.IsNullOrWhiteSpace(cfgJson)))
            {
                this.ClientCfg.LoadConfig(cfgJson);

                if (this.ClientCfg.LoadedConfig["vehicles_enableSeatbelts"] == true)
                {
                    Tick += this.BeltManager.OnTick;
                    Tick += this.BeltManager.DeathTick;
                }

                this.UniformManager.short_sleeves_male = new int[this.ClientCfg.LoadedConfig["eup_shortSleeves_male"].Count];
                this.UniformManager.long_sleeves_male = new int[this.ClientCfg.LoadedConfig["eup_longSleeves_male"].Count];
                this.UniformManager.short_sleeves_female = new int[this.ClientCfg.LoadedConfig["eup_shortSleeves_female"].Count];
                this.UniformManager.long_sleeves_female = new int[this.ClientCfg.LoadedConfig["eup_longSleeves_female"].Count];

                for(int i=0; i < this.ClientCfg.LoadedConfig["eup_shortSleeves_male"].Count; i++)
                {
                    this.UniformManager.short_sleeves_male[i] = (int)this.ClientCfg.LoadedConfig["eup_shortSleeves_male"][i];
                }

                for (int i = 0; i < this.ClientCfg.LoadedConfig["eup_longSleeves_male"].Count; i++)
                {
                    this.UniformManager.long_sleeves_male[i] = (int)this.ClientCfg.LoadedConfig["eup_longSleeves_male"][i];
                }

                for (int i = 0; i < this.ClientCfg.LoadedConfig["eup_shortSleeves_female"].Count; i++)
                {
                    this.UniformManager.short_sleeves_female[i] = (int)this.ClientCfg.LoadedConfig["eup_shortSleeves_female"][i];
                }

                for (int i = 0; i < this.ClientCfg.LoadedConfig["eup_longSleeves_female"].Count; i++)
                {
                    this.UniformManager.long_sleeves_female[i] = (int)this.ClientCfg.LoadedConfig["eup_longSleeves_female"][i];
                }
            }
        }
    }
}
