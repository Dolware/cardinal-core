﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.utils
{
    public class Utils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="time">Time to display in milliseconds</param>
        public static void DrawMissionText(string text, int time)
        {
            API.ClearPrints();
            API.SetTextEntry_2("STRING");
            API.AddTextComponentString(text);
            API.DrawSubtitleTimed(time, true);
        }

        public static void DrawHelpText(string msg)
        {
            Function.Call(Hash.BEGIN_TEXT_COMMAND_DISPLAY_HELP, "STRING");
            Function.Call(Hash._ADD_TEXT_COMPONENT_SCALEFORM, msg);
            Function.Call(Hash.END_TEXT_COMMAND_DISPLAY_HELP, 0, 0, 1, -1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="bg">140=Black | 6=Red | 184=Green</param>
        public static void DrawTextNotification(string text, int bg)
        {
            Function.Call(Hash._SET_NOTIFICATION_TEXT_ENTRY, "STRING");
            Function.Call(Hash._ADD_TEXT_COMPONENT_STRING, text);
            Function.Call(Hash._SET_NOTIFICATION_BACKGROUND_COLOR, bg);
            Function.Call(Hash._DRAW_NOTIFICATION, false, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="bg">140=Black | 6=Red | 184=Green</param>
        public static void DrawAdvancedTextNotification(string title, string text, string picture, int bg, int type)
        {
            API.AddTextEntry("showAdNotification", text);
            API.SetNotificationTextEntry("showAdNotification");
            API.SetNotificationMessage(picture, picture, false, type, title, text);
            API.DrawNotification(false, false);
        }

        public static void DrawLine(Vector3 start, Vector3 end, Color col)
        {
            Function.Call(Hash.DRAW_LINE, start.X, start.Y, start.Z, end.X, end.Y, end.Z, col.R, col.G, col.B, col.A);
        }

        public static float RenderText(String text, int font, float x, float y, float scale, int[] colors)
        {
            API.SetTextFont(font);
            API.SetTextScale(scale, scale);
            API.SetTextColour(colors[0], colors[1], colors[2], colors[3]);
            API.SetTextWrap(0.0f, 1.0f);
            API.SetTextCentre(true);
            API.SetTextDropshadow(5, 0, 0, 0, 255);
            API.SetTextEdge(1, 0, 0, 0, 205);
            API.SetTextEntry("STRING");
            API.AddTextComponentString(text);
            API.SetTextProportional(true);
            API.SetTextRightJustify(true);
            API.DrawText(x, y);
            return API.GetTextScreenWidth(false);
        }

        public static void RenderText2(string text, float x, float y, float width, float height, float scale, int[] colors)
        {
            API.SetTextFont(0);
            API.SetTextProportional(false);
            API.SetTextScale(scale, scale);
            API.SetTextColour(colors[0], colors[1], colors[2], colors[3]);
            API.SetTextDropshadow(0, 0, 0, 0, 255);
            API.SetTextEdge(1, 0, 0, 0, 255);
            API.SetTextDropShadow();
            API.SetTextOutline();
            API.SetTextEntry("STRING");
            API.AddTextComponentString(text);
            API.DrawText(x - width, y - height / 2 + 0.005f);
        }

        public static Player GetClosestPlayer(float maxDistance)
        {
            PlayerList pl = new PlayerList();
            Vector3 PlayerCoords = Game.PlayerPed.Position;
            float closestDist = -1f;
            Player closestPlayer = null;
            foreach (Player p in pl)
            {
                if (p != Game.Player)
                {
                    Vector3 targetCoords = p.Character.Position;
                    float distance = Function.Call<float>(Hash.GET_DISTANCE_BETWEEN_COORDS, targetCoords.X, targetCoords.Y, targetCoords.Z, PlayerCoords.X, PlayerCoords.Y, PlayerCoords.Z);
                    if (closestDist == -1 || closestDist > distance)
                    {
                        if (distance <= maxDistance)
                        {
                            closestPlayer = p;
                            closestDist = distance;
                        }
                    }
                }
            }
            return closestPlayer;
        }

        public static void Render3DText(Vector3 coords, string text, int opacidade, Color col, float scale = 0.54f)
        {
            API.SetTextScale(scale, scale);
            API.SetTextFont(4);
            API.SetTextProportional(true);
            API.SetTextColour(col.R, col.G, col.B, opacidade);
            API.SetTextDropshadow(0, 0, 0, 0, opacidade);
            API.SetTextEdge(2, 0, 0, 0, 150);
            API.SetTextDropShadow();
            API.SetTextOutline();
            API.SetTextCentre(true);

            API.SetDrawOrigin(coords.X, coords.Y, coords.Z, 0);
            API.BeginTextCommandDisplayText("STRING");
            API.AddTextComponentSubstringPlayerName(text);
            API.EndTextCommandDisplayText(0, 0);
            API.ClearDrawOrigin();
        }

        public static async void PlayAnim(string dict, string anim, int duration)
        {
            while(!Function.Call<bool>(Hash.HAS_ANIM_DICT_LOADED, dict))
            {
                Function.Call(Hash.REQUEST_ANIM_DICT, dict);
                await BaseScript.Delay(100);
            }

            int move = 0;

            if(API.IsPedInAnyVehicle(Game.PlayerPed.Handle, false))
            {
                move = 51;
            }

            API.TaskPlayAnim(Game.PlayerPed.Handle, dict, anim, 3.0f, 3.0f, duration, move, 0, false, false, false);
        }
    }
}
