﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client
{
    public class Config
    {
        public string[] SanitizedConfigValues = new string[]
        {
            "discordHook"
        };

        public Dictionary<string, dynamic> LoadedConfig = new Dictionary<string, dynamic>();

        public void LoadConfig(string cfgJson)
        {
            if(cfgJson != null && cfgJson != "")
            {
                Dictionary<string, dynamic> ReceivedCfg = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(cfgJson);
                foreach(string s in this.SanitizedConfigValues)
                {
                    if(ReceivedCfg.ContainsKey(s))
                    {
                        ReceivedCfg.Remove(s);
                    }
                }
                this.LoadedConfig = ReceivedCfg;
            }
        }
    }
}
