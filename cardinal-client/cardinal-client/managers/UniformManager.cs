﻿using cardinal_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.managers
{
    public class UniformManager
    {
        private dynamic saved_arms = null;
        private dynamic saved_hat = null;
        private dynamic saved_hat_text = null;

        public int[] short_sleeves_male = new int[25];
        public int[] long_sleeves_male = new int[25];
        public int[] short_sleeves_female = new int[25];
        public int[] long_sleeves_female = new int[25];

        public void ToggleGloves()
        {
            int curArms = Function.Call<int>(Hash.GET_PED_DRAWABLE_VARIATION, Game.PlayerPed, 3);
            int curTorso = Function.Call<int>(Hash.GET_PED_DRAWABLE_VARIATION, Game.PlayerPed, 11);

            if(Game.PlayerPed.Model.Hash == API.GetHashKey("mp_m_freemode_01"))
            {
                if (this.short_sleeves_male.Contains(curTorso))
                {
                    if (curArms != Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_male_short"]))
                    {
                        this.saved_arms = curArms;
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_male_short"]), 0, 0);
                    }
                    else
                    {
                        if (this.saved_arms != null)
                        {
                            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, this.saved_arms, 0, 0);
                        }
                    }
                }
                else if (this.long_sleeves_male.Contains(curTorso))
                {
                    if (curArms != Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_male_long"]))
                    {
                        this.saved_arms = curArms;
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_male_long"]), 0, 0);
                    }
                    else
                    {
                        if (this.saved_arms != null)
                        {
                            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, this.saved_arms, 0, 0);
                        }
                    }
                }
            }
            else if(Game.PlayerPed.Model.Hash == API.GetHashKey("mp_f_freemode_01"))
            {
                if (this.short_sleeves_female.Contains(curTorso))
                {
                    if (curArms != Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_female_short"]))
                    {
                        this.saved_arms = curArms;
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_female_short"]), 0, 0);
                    }
                    else
                    {
                        if (this.saved_arms != null)
                        {
                            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, this.saved_arms, 0, 0);
                        }
                    }
                }
                else if (this.long_sleeves_female.Contains(curTorso))
                {
                    if (curArms != Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_female_long"]))
                    {
                        this.saved_arms = curArms;
                        Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, Convert.ToInt32(Client.INSTANCE.ClientCfg.LoadedConfig["eup_gloveId_female_long"]), 0, 0);
                    }
                    else
                    {
                        if (this.saved_arms != null)
                        {
                            Function.Call(Hash.SET_PED_COMPONENT_VARIATION, Game.PlayerPed, 3, this.saved_arms, 0, 0);
                        }
                    }
                }
            }

            Utils.PlayAnim("nmt_3_rcm-10", "cs_nigel_dual-10", 1200);
        }

        public void ToggleHat(string dept)
        {
            if (String.IsNullOrWhiteSpace(dept))
            {
                int curHat = Function.Call<int>(Hash.GET_PED_PROP_INDEX, Game.PlayerPed, 0);
                if(curHat != saved_hat)
                {
                    if (this.saved_hat != null && this.saved_hat_text != null)
                    {
                        Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 0, this.saved_hat, this.saved_hat_text, true);
                        Utils.PlayAnim("mp_masks@standard_car@ds@", "put_on_mask", 600);
                    }
                }
                else
                {
                    Function.Call(Hash.CLEAR_PED_PROP, Game.PlayerPed, 0);
                    Utils.PlayAnim("missheist_agency2ahelmet", "take_off_helmet_stand", 1200);
                }
            }
            else
            {
                string selectedHat = "";
                switch (dept)
                {
                    case "sasp":
                        if(Game.PlayerPed.Model.Hash == API.GetHashKey("mp_m_freemode_01"))
                        {
                            selectedHat = Client.INSTANCE.ClientCfg.LoadedConfig["eup_hatId_state_male"];
                        }else if(Game.PlayerPed.Model.Hash == API.GetHashKey("mp_f_freemode_01"))
                        {
                            selectedHat = Client.INSTANCE.ClientCfg.LoadedConfig["eup_hatId_state_female"];
                        }
                        break;
                    case "bcso":
                        if (Game.PlayerPed.Model.Hash == API.GetHashKey("mp_m_freemode_01"))
                        {
                            selectedHat = Client.INSTANCE.ClientCfg.LoadedConfig["eup_hatId_county_male"];
                        }
                        else if (Game.PlayerPed.Model.Hash == API.GetHashKey("mp_f_freemode_01"))
                        {
                            selectedHat = Client.INSTANCE.ClientCfg.LoadedConfig["eup_hatId_county_female"];
                        }
                        break;
                    case "lspd":
                        if (Game.PlayerPed.Model.Hash == API.GetHashKey("mp_m_freemode_01"))
                        {
                            selectedHat = Client.INSTANCE.ClientCfg.LoadedConfig["eup_hatId_city_male"];
                        }
                        else if (Game.PlayerPed.Model.Hash == API.GetHashKey("mp_f_freemode_01"))
                        {
                            selectedHat = Client.INSTANCE.ClientCfg.LoadedConfig["eup_hatId_city_female"];
                        }
                        break;
                }

                if(!(String.IsNullOrWhiteSpace(selectedHat)))
                {
                    string[] h = selectedHat.Split(':');
                    Function.Call(Hash.SET_PED_PROP_INDEX, Game.PlayerPed, 0, Convert.ToInt32(h[0]), Convert.ToInt32(h[1]), true);
                    Utils.PlayAnim("mp_masks@standard_car@ds@", "put_on_mask", 600);

                    this.saved_hat = Convert.ToInt32(h[0]);
                    this.saved_hat_text = Convert.ToInt32(h[1]);
                }
            }
        }
    }
}
