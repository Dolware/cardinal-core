﻿using cardinal_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.managers
{
    public class HudManager
    {
        public void RenderHud()
        {
            if(Game.PlayerPed.IsDead)
            {
                Utils.DrawTextNotification("Press 'R' to revive yourself", 140);
            }

            if(Game.IsControlPressed(0, Control.Phone))
            {
                PlayerList pl = new PlayerList();
                foreach(Player p in pl)
                {
                    if(p != Game.Player)
                    {
                        if (Game.PlayerPed.Position.DistanceToSquared(p.Character.Position) <= Client.INSTANCE.ClientCfg.LoadedConfig["hud_overheadId_range"])
                        {
                            if (API.NetworkIsPlayerTalking(p.Handle))
                            {
                                Utils.Render3DText(new Vector3(p.Character.Position.X, p.Character.Position.Y, p.Character.Position.Z + 1.0f), p.ServerId.ToString(), 255, Color.FromArgb(255, 247, 124, 56), 0.40f);
                            }
                            else
                            {
                                Utils.Render3DText(new Vector3(p.Character.Position.X, p.Character.Position.Y, p.Character.Position.Z + 1.0f), p.ServerId.ToString(), 255, Color.FromArgb(255, 255, 255, 255), 0.40f);
                            }
                        }
                    }
                }
            }
        }
    }
}
