﻿using cardinal_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.managers
{
    public class BallisticManager
    {
        public void Revive()
        {
            if(Game.PlayerPed.IsDead)
            {
                Game.PlayerPed.IsInvincible = false;
                Game.Player.IsInvincible = false;
                API.NetworkResurrectLocalPlayer(Game.PlayerPed.Position.X, Game.PlayerPed.Position.Y, Game.PlayerPed.Position.Z, Game.PlayerPed.Heading, true, false);
                Game.PlayerPed.CancelRagdoll();
                Game.PlayerPed.Resurrect();
                Game.PlayerPed.Health = Game.PlayerPed.MaxHealth;
                Game.PlayerPed.ClearBloodDamage();
            }
        }

        public void Heal()
        {
            Game.PlayerPed.Health = Game.PlayerPed.MaxHealth;
            Game.PlayerPed.ClearBloodDamage();
        }

        public void ReviveClosest()
        {
            Player p = Utils.GetClosestPlayer(2.0f);
            if(p != null)
            {
                BaseScript.TriggerServerEvent("crp:SV_E:Ballistics:RevivePlayer", p.Handle);
            }
        }

        public void HealClosest()
        {
            Player p = Utils.GetClosestPlayer(2.0f);
            if (p != null)
            {
                BaseScript.TriggerServerEvent("crp:SV_E:Ballistics:HealPlayer", p.Handle);
            }
        }

        public void Tick()
        {
            if(Game.PlayerPed.IsDead)
            {
                API.SetPlayerInvincible(API.GetPlayerPed(-1), true);
                API.SetEntityHealth(API.GetPlayerPed(-1), 1);
                if(Game.Player.WantedLevel > 0)
                {
                    Game.Player.WantedLevel = 0;
                }

                if (Game.IsControlJustPressed(0, Control.Reload))
                {
                    this.Revive();
                }
            }
        }
    }
}
