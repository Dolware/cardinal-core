﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.managers
{
    public class VehicleManager
    {
        private float wheel_angle = 0.0f;

        public void OnTick()
        {
            if(API.DoesEntityExist(API.GetVehiclePedIsUsing(Game.PlayerPed.Handle)))
            {
                float steeringAngle = API.GetVehicleSteeringAngle(API.GetVehiclePedIsUsing(Game.PlayerPed.Handle));
                if(steeringAngle > 10.0f || steeringAngle < -10.0f)
                {
                    this.wheel_angle = steeringAngle;
                }

                if(API.GetEntitySpeed(API.GetVehiclePedIsUsing(Game.PlayerPed.Handle)) < 0.1f && API.DoesEntityExist(API.GetVehiclePedIsIn(Game.PlayerPed.Handle, true)) && !API.GetIsTaskActive(Game.PlayerPed.Handle, 151) && !API.GetIsVehicleEngineRunning(API.GetVehiclePedIsIn(Game.PlayerPed.Handle, true)))
                {
                    API.SetVehicleSteeringAngle(API.GetVehiclePedIsIn(Game.PlayerPed.Handle, true), this.wheel_angle);
                }
            }
        }
    }
}
