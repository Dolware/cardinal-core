﻿using cardinal_client.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.managers
{
    public class PoliceManager
    {
        public bool IsPlayerCuffed = false;

        public async void ToggleCuff()
        {
            if(API.IsEntityPlayingAnim(Game.PlayerPed.Handle, "mp_arresting", "idle", 3))
            {
                API.ClearPedSecondaryTask(Game.PlayerPed.Handle);
                API.SetEnableHandcuffs(Game.PlayerPed.Handle, false);
                API.SetCurrentPedWeapon(Game.PlayerPed.Handle, (uint)API.GetHashKey("WEAPON_UNARMED"), true);
                this.IsPlayerCuffed = false;
                API.SetPedPathCanUseLadders(Game.PlayerPed.Handle, true);
                API.DisableControlAction(1, 140, false);
                API.DisableControlAction(1, 141, false);
                API.DisableControlAction(1, 142, false);
                API.DisableControlAction(0, 59, false);
            }
            else
            {
                API.RequestAnimDict("mp_arresting");
                while(!API.HasAnimDictLoaded("mp_arresting"))
                {
                    await BaseScript.Delay(100);
                }

                API.TaskPlayAnim(Game.PlayerPed.Handle, "mp_arresting", "idle", 8.0f, -8, -1, 49, 0, false, false, false);
                API.SetEnableHandcuffs(Game.PlayerPed.Handle, true);
                API.SetCurrentPedWeapon(Game.PlayerPed.Handle, (uint)API.GetHashKey("WEAPON_UNARMED"), true);
                this.IsPlayerCuffed = true;
            }
        }

        public async void ToggleHandsup()
        {
            if(!API.IsEntityPlayingAnim(Game.PlayerPed.Handle, "mp_arresting", "idle", 3))
            {
                API.RequestAnimDict("random@mugging3");
                while(!API.HasAnimDictLoaded("random@mugging3"))
                {
                    await BaseScript.Delay(100);
                }

                if(API.IsEntityPlayingAnim(Game.PlayerPed.Handle, "random@mugging3", "handsup_standing_base", 3))
                {
                    API.ClearPedSecondaryTask(Game.PlayerPed.Handle);
                    API.SetEnableHandcuffs(Game.PlayerPed.Handle, false);
                    API.SetCurrentPedWeapon(Game.PlayerPed.Handle, (uint)API.GetHashKey("WEAPON_UNARMED"), true);
                    BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "", "You have put your hands down" } });
                    API.SetPedPathCanUseLadders(Game.PlayerPed.Handle, true);
                    API.DisableControlAction(1, 140, false);
                    API.DisableControlAction(1, 141, false);
                    API.DisableControlAction(1, 142, false);
                    API.DisableControlAction(0, 59, false);
                }
                else
                {
                    API.TaskPlayAnim(Game.PlayerPed.Handle, "random@mugging3", "handsup_standing_base", 8.0f, -8, -1, 49, 0, false, false, false);
                    API.SetEnableHandcuffs(Game.PlayerPed.Handle, true);
                    API.SetCurrentPedWeapon(Game.PlayerPed.Handle, (uint)API.GetHashKey("WEAPON_UNARMED"), true);
                    BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "", "You have put your hands up" } });
                }
            }
            else
            {
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "", "You cannot put your hands up while cuffed" } });
            }
        }
        
        public void CuffClosest()
        {
            Player p = Utils.GetClosestPlayer(2.0f);
            if (p != null)
            {
                BaseScript.TriggerServerEvent("crp:SV_E:Police:CuffPlayer", p.Handle);
            }
        }

        public async Task OnTick()
        {
            if(Game.Player.WantedLevel > 0)
            {
                Game.Player.WantedLevel = 0;
            }

            if(this.IsPlayerCuffed && !API.IsEntityPlayingAnim(Game.PlayerPed.Handle, "mp_arresting", "idle", 3))
            {
                await BaseScript.Delay(3000);
                API.TaskPlayAnim(Game.PlayerPed.Handle, "mp_arresting", "idle", 8.0f, -8, -1, 49, 0, false, false, false);
            }

            if(API.IsEntityPlayingAnim(Game.PlayerPed.Handle, "mp_arresting", "idle", 3))
            {
                API.DisableControlAction(1, 140, true);
                API.DisableControlAction(1, 141, true);
                API.DisableControlAction(1, 142, true);
                API.SetPedPathCanUseLadders(Game.PlayerPed.Handle, false);
                if(API.IsPedInAnyVehicle(Game.PlayerPed.Handle, false))
                {
                    API.DisableControlAction(0, 59, true);
                }
            }

            if (API.IsEntityPlayingAnim(Game.PlayerPed.Handle, "random@mugging3", "handsup_standing_base", 3))
            {
                API.DisableControlAction(1, 140, true);
                API.DisableControlAction(1, 141, true);
                API.DisableControlAction(1, 142, true);
                API.SetPedPathCanUseLadders(Game.PlayerPed.Handle, false);
                if (API.IsPedInAnyVehicle(Game.PlayerPed.Handle, false))
                {
                    API.DisableControlAction(0, 59, true);
                }
            }
        }
    }
}
