﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using CitizenFX.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.managers
{
    public class BeltManager
    {
        // Some config settings
        private bool showNotification = true;       // Whether to use Screen.ShowNotification on seatbelt status change
        // End configuration

        private bool isUIOpen = false;
        private float[] speedBuffer = new float[2];
        private Vector3[] velBuffer = new Vector3[2];
        private bool beltOn = false;
        private bool wasInCar = false;


        private bool IsVehicle(int v)
        {
            var vc = API.GetVehicleClass(v);

            return (vc >= 0 && vc <= 7) || (vc >= 9 && vc <= 12) || (vc >= 17 && vc <= 20);
        }

        private float[] ForwardVelocity(int ent)
        {
            float hdg = API.GetEntityHeading(ent);
            if (hdg < 0.0) hdg += 360.0f;

            hdg = hdg * 0.0174533f;

            float[] ret = new float[2];
            ret[0] = (float)Math.Cos(hdg) * 2.0f;
            ret[1] = (float)Math.Sin(hdg) * 2.0f;

            return ret;
        }

        public async Task OnTick()
        {
            int _ped = API.GetPlayerPed(-1);
            int _vehicle = API.GetVehiclePedIsIn(_ped, false);
            if (_vehicle > 0 && (wasInCar || IsVehicle(_vehicle)))
            {
                wasInCar = true;
                if (!isUIOpen && !API.IsPlayerDead(_ped))
                {
                    isUIOpen = true;
                    NUIBuckled(beltOn);
                }

                if (beltOn)
                {
                    API.DisableControlAction(0, 75, true);
                }

                speedBuffer[1] = speedBuffer[0];
                speedBuffer[0] = API.GetEntitySpeed(_vehicle);

                if (speedBuffer[1] > 0.0f &&
                    !beltOn &&
                    API.GetEntitySpeedVector(_vehicle, true).Y > 1.0f &&
                    speedBuffer[0] > 15.0f &&
                    (speedBuffer[1] - speedBuffer[0]) > (speedBuffer[0] * 0.255f))
                {
                    var coords = API.GetEntityCoords(_ped, true);
                    var fw = ForwardVelocity(_ped);
                    API.SetEntityCoords(_ped, coords.X + fw[0], coords.Y + fw[1], coords.Z - 0.47f, true, true, true, true);
                    API.SetEntityVelocity(_ped, velBuffer[1].X, velBuffer[1].Y, velBuffer[1].Z);
                    await BaseScript.Delay(1);
                    API.SetPedToRagdoll(API.GetPlayerPed(-1), 3000, 3000, 0, false, false, false);
                }

                velBuffer[1] = velBuffer[0];
                velBuffer[0] = API.GetEntityVelocity(_vehicle);

                if (Game.IsControlJustPressed(0, Control.ReplayShowhotkey))
                {
                    beltOn = !beltOn;
                    if (beltOn && showNotification) Screen.ShowNotification("Seatbelt On");
                    else if (!beltOn && showNotification) Screen.ShowNotification("Seatbelt Off");
                    NUIBuckled(beltOn);
                }
            }
            else if (wasInCar)
            {
                wasInCar = false;
                beltOn = false;
                speedBuffer[0] = speedBuffer[1] = 0.0f;
                if (isUIOpen && !API.IsPlayerDead(_ped))
                {
                    isUIOpen = false;
                }

                NUIBuckled(beltOn);
            }

            await BaseScript.Delay(0);
        }

        public async Task DeathTick()
        {
            if (API.IsPlayerDead(API.PlayerId()) && isUIOpen)
            {
                isUIOpen = false;
            }
            await BaseScript.Delay(100);
        }

        private void NUIBuckled(bool value)
        {
            SendNUIMessage("{\"transactionType\":\"isBuckled\",\"transactionValue\":" + value.ToString().ToLower() +
                           ", \"inCar\":" + wasInCar.ToString().ToLower() + "}");
        }

        private void SendNUIMessage(string message)
        {
            API.SendNuiMessage(message);
        }
    }
}
