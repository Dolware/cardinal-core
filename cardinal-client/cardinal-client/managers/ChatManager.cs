﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_client.managers
{
    public class ChatManager
    {
        private string TwitterHandle = "";


        public void RegisterCommandTips()
        {
            BaseScript.TriggerEvent("chat:addSuggestion", "/gloves", "Toggle your gloves");
            BaseScript.TriggerEvent("chat:addSuggestion", "/g", "Toggle your gloves");
            BaseScript.TriggerEvent("chat:addSuggestion", "/hat", "Toggle your hat", new[]
            {
                new {name="department", help="city/bcso/sasp"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/h", "Toggle your hat", new[]
            {
                new {name="department", help="city/bcso/sasp"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/gooc", "Speak out of character", new[]
            {
                new {name="message", help="Message to be sent"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/ooc", "Speak out of character", new[]
            {
                new {name="message", help="Message to be sent"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "//", "Speak out of character", new[]
            {
                new {name="message", help="Message to be sent"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/looc", "Speak out of character locally", new[]
            {
                new {name="message", help="Message to be sent"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/me", "Perform an action", new[]
            {
                new {name="message", help="Message to be sent"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/do", "Provide some context of your action in the third person", new[]
            {
                new {name="message", help="Message to be sent"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/revive", "Revive the closest player");
            BaseScript.TriggerEvent("chat:addSuggestion", "/heal", "Heal the closest player");
            BaseScript.TriggerEvent("chat:addSuggestion", "/cuff", "Cuff the closest/specified player", new[]
            {
                new {name="id", help="ID of player to cuff if not cuffing the closest player"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/hu", "Put your hands up/down");
            BaseScript.TriggerEvent("chat:addSuggestion", "/twt", "Send a tweet", new[]
            {
                new {name="message", help="Message to be sent"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/twthandle", "Change your Twitter handle", new[]
            {
                new {name="handle", help="New Twitter handle"}
            });
            BaseScript.TriggerEvent("chat:addSuggestion", "/911", "Notify emergency services", new[]
            {
                new {name="message", help="Message to be sent"}
            });
        }

        public void DisplayMessage(int chan, string prefix, dynamic coords, string msg)
        {
            if(coords is Vector3)
            {
                switch (chan)
                {
                    case 0:
                        if (Game.PlayerPed.Position.DistanceToSquared(coords) <= Client.INSTANCE.ClientCfg.LoadedConfig["chat_looc_range"])
                        {
                            BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { prefix, msg } });
                        }
                        break;
                    case 1:
                        if (Game.PlayerPed.Position.DistanceToSquared(coords) <= Client.INSTANCE.ClientCfg.LoadedConfig["chat_me_range"])
                        {
                            BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { prefix, msg } });
                        }
                        break;
                    case 2:
                        if (Game.PlayerPed.Position.DistanceToSquared(coords) <= Client.INSTANCE.ClientCfg.LoadedConfig["chat_action_range"])
                        {
                            BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { prefix, msg } });
                        }
                        break;
                }
            }
            else
            {
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { prefix, msg } });
            }
        }

        public void LocalCallback(int chan, string prefix, string msg)
        {
            BaseScript.TriggerServerEvent("crp:SV_E:Chat:SendLocal", chan, prefix, Game.PlayerPed.Position, msg);
        }

        public void SendTweet(string msg)
        {
            if(!(String.IsNullOrWhiteSpace(this.TwitterHandle)))
            {
                BaseScript.TriggerServerEvent("crp:SV_E:Chat:SendTweet", this.TwitterHandle, msg);
            }
            else
            {
                BaseScript.TriggerServerEvent("crp:SV_E:Chat:SendTweet", "@" + Game.Player.Name, msg);
            }
        }

        public void SetTwitterHandle(string handle)
        {
            if (!(String.IsNullOrWhiteSpace(handle)))
            {
                this.TwitterHandle = "@" + handle;
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^5[Twitter]", "You have set your handle to '" + handle + "'" } });
            }
            else
            {
                BaseScript.TriggerEvent("chat:addMessage", new { color = new[] { 255, 0, 0 }, multiline = true, args = new[] { "^5[Twitter]", "Your handle cannot be blank" } });
            }
        }
    }
}
