﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_server
{
    public class BaseScriptProxy
    {
        public EventHandlerDictionary EventHandlers { get; }
        public ExportDictionary Exports { get; }
        public PlayerList Players { get; }

        public BaseScriptProxy(EventHandlerDictionary eventHandlers, ExportDictionary exports, PlayerList players)
        {
            this.EventHandlers = eventHandlers;
            this.Exports = exports;
            this.Players = players;
        }
    }
}
