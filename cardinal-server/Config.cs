﻿using cardinal_server.utils;
using CitizenFX.Core.Native;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_server
{
    public class Config
    {
        public Dictionary<string, dynamic> LoadedConfig = new Dictionary<string, dynamic>();

        public Config()
        {
            string lf = API.LoadResourceFile(API.GetCurrentResourceName(), "./cardinal_config.json");
            this.LoadedConfig = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(lf);

            Utils.Logging.PrintToConsole("[Config]: Loaded config: " + JsonConvert.SerializeObject(this.LoadedConfig));
        }
    }
}
