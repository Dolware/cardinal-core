﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_server.managers
{
    public class CommandManager
    {
        public static void RegisterAllCommands()
        {
            API.RegisterCommand("gloves", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if(source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];
                    if(p != null)
                    {
                        p.TriggerEvent("crp:CL_E:Uniform:ToggleGloves");
                    }
                }
            }), false);

            API.RegisterCommand("g", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];
                    if (p != null)
                    {
                        p.TriggerEvent("crp:CL_E:Uniform:ToggleGloves");
                    }
                }
            }), false);

            API.RegisterCommand("hat", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];
                    if (p != null)
                    {
                        if(args.Count > 0)
                        {
                            p.TriggerEvent("crp:CL_E:Uniform:ToggleHat", args[0].ToString().ToLower());
                        }
                        else
                        {
                            p.TriggerEvent("crp:CL_E:Uniform:ToggleHat", "");
                        }
                    }
                }
            }), false);

            API.RegisterCommand("h", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];
                    if (p != null)
                    {
                        if (args.Count > 0)
                        {
                            p.TriggerEvent("crp:CL_E:Uniform:ToggleHat", args[0].ToString().ToLower());
                        }
                        else
                        {
                            p.TriggerEvent("crp:CL_E:Uniform:ToggleHat", "");
                        }
                    }
                }
            }), false);

            API.RegisterCommand("ooc", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];

                        foreach (Player p in pl)
                        {
                            p.TriggerEvent("crp:CL_E:Chat:DisplayMessage", -1, "^9[OOC] " + sourcePlayer.Name, -1, FullMsg);
                        }
                    }
                }
            }), false);

            API.RegisterCommand("gooc", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if(args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];

                        foreach (Player p in pl)
                        {
                            p.TriggerEvent("crp:CL_E:Chat:DisplayMessage", -1, "^9[OOC] " + sourcePlayer.Name, -1, FullMsg);
                        }
                    }
                }
            }), false);

            API.RegisterCommand("/", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];

                        foreach (Player p in pl)
                        {
                            p.TriggerEvent("crp:CL_E:Chat:DisplayMessage", -1, "^9[OOC] " + sourcePlayer.Name, -1, FullMsg);
                        }
                    }
                }
            }), false);

            API.RegisterCommand("looc", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];

                        sourcePlayer.TriggerEvent("crp:CL_E:Chat:HandleLocal", 0, "^9[LOOC] " + sourcePlayer.Name, FullMsg);
                    }
                }
            }), false);

            API.RegisterCommand("me", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];

                        sourcePlayer.TriggerEvent("crp:CL_E:Chat:HandleLocal", 1, "", "^3 > " + sourcePlayer.Name + " ^3" + FullMsg);
                    }
                }
            }), false);

            API.RegisterCommand("do", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];

                        sourcePlayer.TriggerEvent("crp:CL_E:Chat:HandleLocal", 2, "", "^4** " + FullMsg);
                    }
                }
            }), false);

            API.RegisterCommand("revive", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];
                    if (p != null)
                    {
                        p.TriggerEvent("crp:CL_E:Ballistics:ReviveClosest");
                    }
                }
            }), false);

            API.RegisterCommand("heal", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];
                    if (p != null)
                    {
                        p.TriggerEvent("crp:CL_E:Ballistics:HealClosest");
                    }
                }
            }), false);

            API.RegisterCommand("cuff", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];

                    if (!(args.Count > 0))
                    {
                        if (p != null)
                        {
                            p.TriggerEvent("crp:CL_E:Police:CuffClosest");
                        }
                    }
                    else
                    {
                        Player targetPl = pl[Convert.ToInt32(args[0])];
                        if(targetPl != null)
                        {
                            targetPl.TriggerEvent("crp:CL_E:Police:ToggleCuff");
                        }
                    }
                }
            }), false);

            API.RegisterCommand("hu", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    PlayerList pl = new PlayerList();
                    Player p = pl[source];
                    if (p != null)
                    {
                        p.TriggerEvent("crp:CL_E:Police:ToggleHandsup");
                    }
                }
            }), false);

            API.RegisterCommand("twt", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];
                        sourcePlayer.TriggerEvent("crp:CL_E:Chat:SendTweet", FullMsg);
                    }
                }
            }), false);

            API.RegisterCommand("twthandle", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();
                        Player sourcePlayer = pl[source];

                        sourcePlayer.TriggerEvent("crp:CL_E:Chat:SetTwitterHandle", args[0]);
                    }
                }
            }), false);

            API.RegisterCommand("911", new Action<int, List<object>, string>((source, args, raw) =>
            {
                if (source > 0)
                {
                    if (args.Count > 0)
                    {
                        PlayerList pl = new PlayerList();

                        string FullMsg = "";
                        foreach (string arg in args)
                        {
                            FullMsg = FullMsg + arg + " ";
                        }
                        Player sourcePlayer = pl[source];

                        foreach (Player p in pl)
                        {
                            p.TriggerEvent("crp:CL_E:Chat:DisplayMessage", -1, "^5[911](" + sourcePlayer.Handle + ")", -1, FullMsg);
                        }
                    }
                }
            }), false);
        }
    }
}
