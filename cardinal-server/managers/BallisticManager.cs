﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_server.managers
{
    public class BallisticManager
    {
        public void RevivePlayer(int id)
        {
            PlayerList pl = new PlayerList();
            Player p = pl[id];
            if(p != null)
            {
                p.TriggerEvent("crp:CL_E:Ballistics:Revive");
            }
        }

        public void HealPlayer(int id)
        {
            PlayerList pl = new PlayerList();
            Player p = pl[id];
            if (p != null)
            {
                p.TriggerEvent("crp:CL_E:Ballistics:Heal");
            }
        }
    }
}
