﻿using CitizenFX.Core;
using CitizenFX.Core.Native;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_server.utils
{
    public class Utils
    {
        public static Logging Logging = new Logging();

        public static StreamWriter getFile(String name, String extension = "txt", Boolean create = true)
        {

            String path = API.GetResourcePath(API.GetCurrentResourceName()) + "/" + name + "." + extension;
            if (!File.Exists(path) && !create)
            {
                return null;
            }

            if (File.Exists(path))
            {
                return new StreamWriter(@path, true);
            }
            else
            {
                return File.CreateText(path);

            }

        }

        public static Dictionary<string, string> GetProperPlayerIDs(Player p)
        {
            Dictionary<string, string> ids = new Dictionary<string, string>();
            foreach (string s in p.Identifiers)
            {
                if (s.ToLower().Contains("license:"))
                {
                    ids.Add("license", s);
                }
                else if (s.ToLower().Contains("steam:"))
                {
                    ids.Add("steam", s);
                }
            }
            return ids;
        }
    }
}
