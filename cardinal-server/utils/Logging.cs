﻿using CitizenFX.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_server.utils
{
    public class Logging
    {
        public void PrintToConsole(string msg)
        {
            if (msg == null || msg == "")
            {
                CitizenFX.Core.Debug.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][CARDINAL] => Invalid message provided in: Logging.PrintToConsole(string msg)");
            }
            else
            {
                StreamWriter sw = this.getFile();
                sw.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][CARDINAL] => " + msg);
                sw.Close();
                CitizenFX.Core.Debug.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "][CARDINAL] => " + msg.Replace('^', '&'));
            }
        }

        public void PrintToDiscord(string msg)
        {
            if (Server.INSTANCE.ServerCfg.LoadedConfig["useDiscord"] == true)
            {
                Server.INSTANCE.BaseScriptProxy.Exports["cardinal-core"].SendToDiscord(Server.INSTANCE.ServerCfg.LoadedConfig["discordHook"], 16753920, "Cardinal-Core", msg, DateTime.Now.ToString());
            }
        }

        public StreamWriter getFile()
        {
            String name = DateTime.Now.ToString("yyyy-MM-dd");

            return Utils.getFile("logs/" + name, "log");
        }
    }
}
