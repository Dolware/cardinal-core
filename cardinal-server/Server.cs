﻿using cardinal_server.managers;
using cardinal_server.utils;
using CitizenFX.Core;
using CitizenFX.Core.Native;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cardinal_server
{
    public class Server : BaseScript
    {
        public static Server INSTANCE { get; set; }

        public Config ServerCfg = new Config();

        #region Class Instances
        public BallisticManager BallisticManager = new BallisticManager();

        public PoliceManager PoliceManager = new PoliceManager();
        #endregion

        public BaseScriptProxy BaseScriptProxy { get; set; }
        
        public Server()
        {
            Server.INSTANCE = this;
            Debug.WriteLine(" ");
            Debug.WriteLine("==========================================================");
            Debug.WriteLine(" ");

            Utils.Logging.PrintToConsole("[Core]: Initializing...");

            Utils.Logging.PrintToConsole("[Core]: Registering events...");
            #region Event Registry
            EventHandlers.Add("crp:SV_E:PlayerReady", new Action<Player>(this.OnPlayerReady));
            EventHandlers.Add("crp:SV_E:Chat:SendLocal", new Action<int, string, Vector3, string>(this.OnLocalCallback));
            EventHandlers.Add("crp:SV_E:Chat:SendTweet", new Action<string, string>(this.OnTweet));

            EventHandlers.Add("crp:SV_E:Ballistics:RevivePlayer", new Action<int>(this.BallisticManager.RevivePlayer));
            EventHandlers.Add("crp:SV_E:Ballistics:HealPlayer", new Action<int>(this.BallisticManager.RevivePlayer));

            EventHandlers.Add("crp:SV_E:Police:CuffPlayer", new Action<int>(this.PoliceManager.CuffPlayer));
            #endregion

            this.BaseScriptProxy = new BaseScriptProxy(this.EventHandlers, this.Exports, this.Players);

            Utils.Logging.PrintToConsole("[Core]: Registering commands...");

            CommandManager.RegisterAllCommands();

            Utils.Logging.PrintToConsole("[Core]: Finished!");

            Debug.WriteLine(" ");
            Debug.WriteLine("==========================================================");
            Debug.WriteLine(" ");
        }

        private void OnPlayerReady([FromSource]Player p)
        {
            p.TriggerEvent("crp:CL_E:LoadConfig", JsonConvert.SerializeObject(this.ServerCfg.LoadedConfig));
        }

        private void OnTweet(string handle, string msg)
        {
            PlayerList pl = new PlayerList();
            foreach (Player p in pl)
            {
                p.TriggerEvent("crp:CL_E:Chat:DisplayMessage", -1, "^5[Tweet](" + handle + ")", -1, msg);
            }
        }

        private void OnLocalCallback(int chan, string prefix, Vector3 coords, string msg)
        {
            PlayerList pl = new PlayerList();
            foreach (Player p in pl)
            {
                p.TriggerEvent("crp:CL_E:Chat:DisplayMessage", chan, prefix, coords, msg);
            }
        }
    }
}
